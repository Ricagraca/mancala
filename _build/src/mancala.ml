
	open Array
	open List
	open Printf
	open ANSITerminal
	open Minimax



	(* Small Functions *)
	let int_of_bool = (function true -> 1 | false -> 0)
	let bool_of_int = (fun i -> if i = 0 then false else true)



	(* Exception *)
	exception Invalid_position of string



	(* Game related *)
	let side_size = 6
	let bean_num = 4
	let size_of_board = 2*(side_size+1)

	let is_mancala i = 
		i = side_size || i = 2*side_size+1

	(* [0-side_size[ false ]side_size-size_of_board-1[ true *)
	let in_user_side i user = 
		(i < side_size && not user) || 
		(i > side_size && i < size_of_board - 1 && user)

	(* Initial number of beans on the bin *)
	let bean_on_bin i =
		if is_mancala i then 
			0
		else 
			bean_num



	(* Returns a mancala board, with the score of user 1 *)
	let board () =
		(Array.init size_of_board bean_on_bin, false)


	(* Put beans in a sequence, since |B|=48 could be a for *)
	let play (game : (int array * bool)) (index : int) (user : bool) : bool =

		let (board,turn) = game in 
		let bean_bin_num = board.(index) in 

		let no_bean = bean_bin_num = 0 in 
		let mancala_index = (side_size + 1) * (int_of_bool user) + side_size in
		let mancala_index_enemy = (side_size + 1) * (int_of_bool (not user)) + side_size in
		let landed_index = (index+bean_bin_num) mod size_of_board in 

		(* Take beans of the bin *)
		board.(index) <- 0;

		let a = ref 1 in 
		let b = ref bean_bin_num in 
		while 0 != (!b) do
			let index_mod = (index+(!a)) mod size_of_board in 
			
			if index_mod != mancala_index_enemy then (
				board.(index_mod) <- board.(index_mod) + 1;
				decr(b)
			);

			incr(a);
		done;

		(* See if it captures *)
		let on_side = in_user_side landed_index user in 
		let one_bean = board.((bean_bin_num+index) mod size_of_board) = 1 in 
		let is_captured = one_bean && on_side in 

		if is_captured then (
			let other_side_index = (2*side_size - landed_index) in
			let other_side_bean = board.(other_side_index) in 
			if other_side_bean > 0 then (
				board.(landed_index) <- 0;
				board.(other_side_index) <- 0;
				board.(mancala_index) <- 1 + other_side_bean + board.(mancala_index)
			)
		);

		(* See if it lands on the mancala of user *)	
		let land_on_mancala = mancala_index = landed_index in 
		let free_turn = land_on_mancala || no_bean in 
		free_turn


	(* Did it finished? *)
	let finish (game : (int array * bool)) : bool = 
		let (board,turn) = game in 
		let a = sub board 0 side_size in 
		let b = sub board (side_size+1) side_size in 
		let f = fun acc e -> if acc then true else bool_of_int e in 
		let at_least_one_bean_a = Array.fold_left f false a in 
		let at_least_one_bean_b = Array.fold_left f false b in 
		not (at_least_one_bean_a && at_least_one_bean_b)


	(* Make a move, and returns true if free turn *)
	let move (game : (int array * bool)) (position : int) (user : bool) : (int array * bool) = 
		let (board,turn) = game in 
		(* Check if position is in range *)
		if position < 0 || position >= side_size then (
			raise (Invalid_position("Invalide index passed!"))
		);

		let cp_board = Array.copy board in 
		let index = (side_size + 1) * (int_of_bool user) + position in
		let free_turn = play (cp_board,turn) index user in 
		cp_board,(if free_turn then turn else not turn)


	(* Return possible moves for player *)
	let possible_moves (game : (int array * bool)) (user : bool) : (int list) = 
		let (board,turn) = game in 
		let starting_index = if not user then 0 else (side_size+1) in 
		let positions = sub board starting_index side_size in 
		let list_of_indexes = to_list positions in 
		let positions_with_index = mapi (fun i e -> (i,e)) list_of_indexes in 
		let filtered_positions = filter (fun e -> snd e > 0) positions_with_index in 
		let indexes = map (fun e -> fst e) filtered_positions in 
		indexes


	(* Return possible moves for player *)
	let value (game : (int array * bool)) (weight : int) : int = 
		let (board,turn) = game in 
		let positions_1 = sub board 0 (side_size+1) in 
		let sum_of_beans_1 = Array.fold_left (+) 0 positions_1 in 

		let positions_2 = sub board (side_size+1) (side_size+1) in 
		let sum_of_beans_2 = Array.fold_left (+) 0 positions_2 in 

		let delta = (positions_1.(side_size)) - (positions_2.(side_size)) in 

		sum_of_beans_1 - sum_of_beans_2 + weight * delta


	let who_turn (game : (int array * bool)) : bool = 
		snd game


	(* Create a computer player *)
	module Mancala = Minimax(struct
		type state = (int array * bool)
		type action = int
		let possible_moves = possible_moves
		let move = move
		let value = (fun g -> value g 4)
		let finish = finish
		let who_turn = who_turn
	end);;


	let print_board (game : (int array * bool)) : unit = 
		
		let (board,turn) = game in 
		let print_number i c = 
			match board.(i) < 10 with 
			| true  -> printf [c] " 0%d" (board.(i));
			| false -> printf [c] " %d" (board.(i)) 
		in 
				
		for i=(size_of_board-2) downto (side_size+1) do 
			print_number i red
		done;

		print_newline();
		print_number (size_of_board-1) red;

		for i=0 to (side_size-1) do 
			printf [] "  "
		done;

		print_number side_size green;
		print_newline();

		for i=0 to (side_size-1) do 
			print_number i green 
		done;
		print_newline()


	let rec start_playing (game : (int array * bool)) = 
		let (board,turn) = game in 
		let score = (value game 0) in 
		printf [cyan] " Score: %d\n" (score);
		print_board game;

		if finish game then (
			print_newline ();
			if score > 0 then (
				printf [green] " You win! Congratulations!\n"
			)
			else(
				printf [red] " You lost! Better luck next time!\n"
			);
			exit(0)
		);

		if (not turn) then 
			(printf [yellow] " Your turn: ")
		else
			(printf [yellow] " Bot turn: ");
		let p = 
			match turn with 
			| false -> read_int () 
			| true  -> 
				match Mancala.best_action game true with 
				| Some(e) -> e
				| None -> exit(0) in 
		let new_board = move game p turn in 
		if (turn) then (printf [blue] " %d\n" (p));

		print_newline ();
		start_playing new_board


	let _ = 
		start_playing (board());;



