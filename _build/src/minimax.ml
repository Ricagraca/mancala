
	open List

	let max_depth = 11

	let shuffle d =
	    let nd = List.map (fun c -> (Random.bits (), c)) d in
	    let sond = List.sort compare nd in
	    List.map snd sond

	module type Game = sig
		type state
		type action 
		val possible_moves : state -> bool -> action list
		val move : state -> action -> bool -> state
		val value : state -> int 
		val finish : state -> bool 
		val who_turn : state -> bool 
	end ;;

	module Minimax(M : Game) = struct
	let best_action state turn = 
		let rec minimax si ti depth = 
			let auxiliar_function operation player_turn = fun acc e ->  
		        let current_value,current_action = acc in 
		        let sj = M.move si e player_turn in 
		        let value,_ = minimax sj (M.who_turn sj) (depth+1) in
		        if operation value current_value then value,Some e else acc in 
			match M.finish si || depth >= max_depth with 
			| true -> (M.value si,None)
			| false -> begin 
				let moves = shuffle (M.possible_moves si ti) in 
				match ti with 
				| false  -> fold_left (auxiliar_function (>) (false)) (min_int,None) moves
				| true -> fold_left (auxiliar_function (<) (true)) (max_int,None) moves
		    end
		in let _,action = minimax state turn 0 in action
	end ;;
